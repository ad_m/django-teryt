=====
TERYT
=====

TERYT is a simple Django app to provide TERYT data. It include tools for importing it.

Quick start
-----------

1. Add "teryt" to your INSTALLED_APPS setting like this:

```
INSTALLED_APPS+ = ('teryt',)
```

3. Run `python manage.py migrate` to create the TERYT models (South migration supported).

4. Download TERC.xml from http://www.stat.gov.pl/broker/access/prefile/listPreFiles.jspa

5. Run `python magee.py gen_terc TERC.xml` or fg. `python mange.py gen_terc TERC.xml` -e 4 -e 5 -e 9` for exclude "miasto w gminie miejsko-wiejskiej", "obszar wiejski w gminie miejsko-wiejskiej", "dzielnica w m.st. Warszawa", "delegatury w gminach miejskich".

4. Create ForeignKey etc. to model teryt.Teryt.