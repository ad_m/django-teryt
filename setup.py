import os
from setuptools import setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-teryt',
    version='0.1',
    packages=['teryt'],
    include_package_data=True,
    license='MIT License',  # example license
    description='A simple Django app to conduct TERYT data.',
    long_description=README,
    url='http://jawne.info.pl/',
    author='Adam Dobrawy',
    author_email='naczelnik@jawnosc.tk',
    install_requires = ['django', 'django-mptt'],
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License', # example license
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)