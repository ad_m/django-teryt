# -*- coding: utf-8 -*-
from lxml import etree
from teryt.models import Teryt
from django.core.management.base import BaseCommand, CommandError
from optparse import make_option


class Command(BaseCommand):
    help = ('Creates a data in database base on Teryt.xml file.')
    option_list = BaseCommand.option_list + (
        make_option("-e", type="string", action="append",
                  dest="exclude", help="Excluded rodz (suggest: 4,5,9)"),
        )

    def handle(self, *args, **options):
        if len(args) < 1:
            raise CommandError('Necessary [file] argument ommited.')
        try:
            root = etree.parse(args[0])
        except IOError:
            raise CommandError('Unable to load file "%s"' % args[0])
        self.stdout.write(("Importing started. "
                           "This may take a few seconds. Please wait a moment.\n"))
        woj = {}
        pw = {}
        rows_count = 0
        for row in root.iter('row'):
            # Mam nadzieje, że sortowanie jest z jakimiś resztkami rozsądku.
            # Pewnie pod CSV tak.
            if not row[3].text is None and row[3].text in options['exclude']:
                    self.stdout.write("Skipped %s\n" % (row[4].text))
                    continue
            if row[1].text is None:  # województwo
                woj[row[0].text] = Teryt.objects.create(woj=row[0].text,
                                                       nazwa=row[4].text,
                                                       nazdod=row[5].text,
                                                       stan_na=row[6].text)
                pw[row[0].text] = {}
            elif row[2].text is None:  # powiat
                pw[row[0].text][row[1].text] = Teryt.objects.create(
                    parent=woj[row[0].text],
                    woj=row[0].text,
                    pow=row[1].text,
                    nazwa=row[4].text,
                    nazdod=row[5].text,
                    stan_na=row[6].text)
            else:  # gmina
                Teryt.objects.create(parent=pw[row[0].text][row[1].text],
                                    woj=row[0].text,
                                    pow=row[1].text,
                                    gmi=row[2].text,
                                    rodz=row[3].text,
                                    nazwa=row[4].text,
                                    nazdod=row[5].text,
                                    stan_na=row[6].text)
                rows_count = rows_count + 1
        self.stdout.write("Teryt %s rows imported.\n" % rows_count)
