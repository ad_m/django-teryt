# -*- coding: utf-8 -*-
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from model_utils import Choices
from autoslug import AutoSlugField


class Teryt(MPTTModel):
    parent = TreeForeignKey('self', null=True, blank=True,
                            related_name='children')
    RODZ = Choices((1, 'gmina_miejska', 'gmina miejska'),
                  (2, 'gmina_wiejska', 'gmina wiejska'),
                  (3, 'gmina_miejsko_wiejska', 'gmina miejsko-wiejska'),
                  (4, 'miasto_w_gminie', 'miasto w gminie miejsko-wiejskiej'),
                  (5, 'obszar_wiejski', 'obszar wiejski w gminie miejsko-wiejskiej'),
                  (8, 'dzielnica', 'dzielnica w m.st. Warszawa'),
                  (9, 'delegatura', 'delegatura gminy miejskiej'),)
    woj = models.IntegerField("Symbol województwa", max_length=2, default=0)
    pow = models.IntegerField("Symbol powiatu", max_length=2, default=0)
    gmi = models.IntegerField("Symbol gminy", max_length=2, default=0)
    rodz = models.IntegerField("Rodzaj jednostki", choices=RODZ, default=0)
    nazwa = models.CharField("Nazwa", max_length=36,)
    nazdod = models.CharField("Określenie jednostki", max_length=50, )
    stan_na = models.DateField("Data aktualizacji w systemie TERC",)
    LEVEL = Choices((0, 'voivodeship', 'województwo'),
                   (1, 'county', 'powiat'),
                   (2, 'community', 'gmina'),)
    level = models.IntegerField("Poziom", choices=LEVEL)
    slug = AutoSlugField(populate_from='nazwa')

    def code(self):
        return '%02d%02d%02d%01d' % (int(self.woj), int(self.pow), int(self.gmi), int(self.rodz))

    @classmethod 
    def by_code(self, code):  # TODO: Manager
        return self.objects.filter(woj=code[0:2]).\
            filter(pow=code[2:4]).\
            filter(gmi=code[4:6]).\
            filter(rodz=code[6])

    @classmethod 
    def by_level(self, level):  # TODO: Manager
        return self.objects.filter(level=getattr(self.model.LEVEL, level))

    def level_friendly(self):
        return self.LEVEL[self.level][1]
    code.short_description = "Kod GUS"

    def rodz_friendly(self):
        for i in self.RODZ._full:
            if i[0] == int(self.rodz):
                return i
        return False

    @staticmethod
    def autocomplete_search_fields():
        return ("nazwa__icontains",)

    def __unicode__(self):
        return self.nazwa

    class MPTTMeta:
        order_insertion_by = ['nazwa']

    class Meta:
        verbose_name = "TERYT"
        verbose_name_plural = "TERYT"
