# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Teryt'
        db.create_table(u'teryt_teryt', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('parent', self.gf('mptt.fields.TreeForeignKey')(blank=True, related_name='children', null=True, to=orm['teryt.Teryt'])),
            ('woj', self.gf('django.db.models.fields.IntegerField')(default=0, max_length=2)),
            ('pow', self.gf('django.db.models.fields.IntegerField')(default=0, max_length=2)),
            ('gmi', self.gf('django.db.models.fields.IntegerField')(default=0, max_length=2)),
            ('rodz', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('nazwa', self.gf('django.db.models.fields.CharField')(max_length=36)),
            ('nazdod', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('stan_na', self.gf('django.db.models.fields.DateField')()),
            ('level', self.gf('django.db.models.fields.IntegerField')()),
            (u'lft', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            (u'rght', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            (u'tree_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
        ))
        db.send_create_signal(u'teryt', ['Teryt'])


    def backwards(self, orm):
        # Deleting model 'Teryt'
        db.delete_table(u'teryt_teryt')


    models = {
        u'teryt.teryt': {
            'Meta': {'object_name': 'Teryt'},
            'gmi': ('django.db.models.fields.IntegerField', [], {'default': '0', 'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.IntegerField', [], {}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'nazdod': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'nazwa': ('django.db.models.fields.CharField', [], {'max_length': '36'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['teryt.Teryt']"}),
            'pow': ('django.db.models.fields.IntegerField', [], {'default': '0', 'max_length': '2'}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'rodz': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'stan_na': ('django.db.models.fields.DateField', [], {}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'woj': ('django.db.models.fields.IntegerField', [], {'default': '0', 'max_length': '2'})
        }
    }

    complete_apps = ['teryt']