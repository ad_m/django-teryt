# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Teryt.slug'
        db.add_column(u'teryt_teryt', 'slug',
                      self.gf('autoslug.fields.AutoSlugField')(default='a', unique_with=(), max_length=50, populate_from='nazwa'),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Teryt.slug'
        db.delete_column(u'teryt_teryt', 'slug')


    models = {
        u'teryt.teryt': {
            'Meta': {'object_name': 'Teryt'},
            'gmi': ('django.db.models.fields.IntegerField', [], {'default': '0', 'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.IntegerField', [], {}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'nazdod': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'nazwa': ('django.db.models.fields.CharField', [], {'max_length': '36'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['teryt.Teryt']"}),
            'pow': ('django.db.models.fields.IntegerField', [], {'default': '0', 'max_length': '2'}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'rodz': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'populate_from': "'nazwa'"}),
            'stan_na': ('django.db.models.fields.DateField', [], {}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'woj': ('django.db.models.fields.IntegerField', [], {'default': '0', 'max_length': '2'})
        }
    }

    complete_apps = ['teryt']